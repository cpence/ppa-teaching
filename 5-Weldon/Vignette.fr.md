---
lang: fr
---

# 5. W.F.R. Weldon, gènes et traits

![Gregor Mendel, dans Bateson, 1902, (image : domaine public, Wikimedia Commons)](mendel.jpg){.smallimg .left width=33% footnote="William Bateson, *Mendel's Principles of Heredity: A Defence: With a Translation of Mendel's Original Papers on Hybridisation*, Cambridge, 1902, éd. Cambridge University Press. <https://doi.org/10.5962/bhl.title.95694>."}

Comme vous le savez sans doute, un moine du nom de [Gregor Mendel](https://fr.wikipedia.org/wiki/Gregor_Mendel) (1822-1884) a effectué au milieu du XIX^e^ siècle une série d'expériences sur [divers caractères présents dans les pois](https://www.ncbi.nlm.nih.gov/books/NBK22098/), qui devaient se révéler centrales dans le développement de la science que nous appelons aujourd'hui génétique. Mendel a montré qu'un certain nombre des traits de ces pois, comme leur couleur (jaune ou verte), leur aspect (lisse ou ridé) et leur forme (ronde ou irrégulière), était transmis des parents aux descendants d'une façon très spécifique. Pour utiliser notre terminologie moderne, chaque parent semble porteur de deux allèles pour chaque locus, c'est-à-dire deux « versions » du caractère, et en transmet un (déterminé au hasard) à chaque descendant.

De plus, le lien entre ces deux allèles et l'apparence des pois semblait exceptionnellement clair. Certains traits, la couleur jaune plutôt que verte ou l'aspect lisse plutôt que ridé, sont ce qu'il a appelé « dominants » : si un pois a une seule copie de l'allèle jaune, il aura la couleur jaune. L'autre trait de chaque paire est ce qu'il a appelé « récessif » : un pois a besoin de **deux** exemplaires de l'allèle vert pour être vert. Mendel rend compte de ceci en disant que la couleur jaune est dominante par rapport à la verte dans les cas où elles sont mélangées.

:::: think
**RÉFLEXION 1 :** Que connaissez-vous déjà sur Mendel et ses travaux ? Comment était-il présenté dans votre manuel ?
::::

![Une gousse de *Pisum sativum* (image : CC-BY-SA, Rasbak, Wikimedia Commons)](pisum.jpg){.smallimg width=33%}

Voilà ce qu'on trouve dans tous les livres d'histoire et de biologie. Ce que votre manuel n'expliquait peut-être pas est que, quand Mendel publie son étude en 1865, elle passe presque totalement inaperçue. Ce n'est qu'en 1900, quand des biologistes appelés [Hugo De Vries](https://fr.wikipedia.org/wiki/Hugo_de_Vries) (1848-1935), [Carl Correns](https://fr.wikipedia.org/wiki/Carl_Correns) (1864-1933) et [Erich von Tschermak](https://fr.wikipedia.org/wiki/Erich_von_Tschermak-Seysenegg) (1871-1962) cherchent, chacun de son côté, [des « sauts » dans certains caractères](https://en.wikipedia.org/wiki/Mutationism) comme le passage du jaune au vert ou d'un pois lisse à un pois irrégulier, qu'ils découvrent tous les trois l'article de Mendel. Le succès est immédiat. Depuis Darwin, comme nous l'avons vu à plusieurs reprises dans les leçons précédentes, les biologistes s'inquiétaient de ne pas réellement disposer d'une théorie pouvant expliquer les variations chez les organismes et comment elles étaient transmises des parents aux enfants. En l'absence d'une théorie de ce genre, comment comprendre vraiment le fonctionnement de la sélection naturelle ? Serait-il même possible de vraiment comprendre un jour l'évolution ?

Une partie des biologistes considéra que l'article de Mendel fournissait la réponse facile rêvée à cette question. Chaque caractère de chaque organisme, du jaune d'un pois à l'intelligence d'une personne, serait tout simplement gouverné par un petit nombre de gènes. Si on pouvait comprendre ces gènes, on pourrait les contrôler : découvrir le [« gène de la tomate qui a du goût »](https://www.smithsonianmag.com/smart-news/tastier-tomatoes-may-be-making-comeback-180972175/) nous permettrait de faire pousser les meilleures tomates du monde.

Cependant, nous le savons aujourd'hui, la génétique est **beaucoup** plus compliquée que ça. Pour presque n'importe quel trait, le nombre de gènes concernés est **énorme** et ces gènes ne peuvent pas être déconnectés de l'environnement dans lequel l'organisme se développe. De nombreux gènes sont impliqués dans des interrelations extrêmement complexes, où des réseaux de différents gènes sont responsables de la « régulation » de la production du reste du réseau, avec certains gènes permettant à d'autres de s'exprimer, etc.

:::: think
**RÉFLEXION 2 :** Il est important de ne pas oublier que, quand le travail de Mendel a été publié initialement et évalué, on n'avait pas encore découvert les bases de la biologie et la biochimie cellulaires ou l'ADN. Quels types de données peut-on rassembler dans ce cas et comment peut-on les utiliser pour constituer une théorie scientifique ? Quelles seraient les limites du type de théories productibles avec cette méthode ?
::::

![W.F.R. Weldon (image : domaine public, dans Pearson[^4] \[1906\], image scannée par Charles Pence)](weldon.jpg){.smallimg .left width=33% footnote="Karl Pearson, « Walter Frank Raphael Weldon. 1860--1906 », *Biometrika*, 1906, vol. 5, n^o^ 1-2, p. 1-52, <https://doi.org/10.1093/biomet/5.1-2.1>."}

Le point que nous étudierons aujourd'hui fait partie des premières pages de la découverte de la complexité de la génétique et l'objectif est de réfléchir à un des concepts fondamentaux à l'œuvre dans les toutes premières expériences de Mendel : **qu'est-ce qu'un gène ?** Comment a-t-on décidé ce qui constituait un « trait » unique ? Comment prendre cette décision dans un monde qui ne connaît ni la biochimie ni l'ADN et où on peut seulement observer les organismes autour de soi ? Et quelle est la relation entre l'approche de Mendel vis à vis de la génétique, où on observe la configuration génétique de deux individus et des descendants qu'ils peuvent avoir, et une perspective évolutive, où on s'intéresse aux **populations** d'organismes ?

À mesure que l'article de Mendel devenait plus célèbre, un nombre croissant de biologistes commença à s'y intéresser. Ce fut le cas de [W.F.R. Weldon](https://fr.wikipedia.org/wiki/Walter_Frank_Raphael_Weldon) (dit Raphael Weldon, 1860-1906), un jeune zoologue spécialiste des invertébrés travaillant à Londres. Il avait passé les années précédentes à développer avec plusieurs collègues [une nouvelle approche des populations évolutives](https://www.britannica.com/science/probability/Biometry) et était une des premières personnes à avoir utilisé les mathématiques, en particulier les statistiques, pour réfléchir sérieusement à l'évolution.

:::: think
**RÉFLEXION 3 :** Darwin a introduit l'intégralité de sa théorie de l'évolution essentiellement sans aucun recours aux mathématiques. Pourquoi, selon vous, l'introduction en biologie de méthodes mathématiques était-elle bienvenue dans la pratique de la discipline aux yeux d'une partie des biologistes mais vue par d'autres comme une complexité inutile et une mauvaise idée ?
::::

Initialement, à la lecture de l'étude de Mendel, Weldon est enthousiaste. Il écrit à un autre scientifique qu'il pense que les données de Mendel pourraient être une excellente manière de comprendre les caractères comme la couleur des yeux chez les êtres humains, où un nombre limité de « types » semble présent dans la nature (yeux bleus, yeux verts, yeux bruns, etc.) et où les types ne semblent pas souvent se mélanger. Mais une étude plus poussée des pois le fait changer d'avis. Il l'a expliqué en détail dans un article de revue publié en 1902 :

:::: {.quote title="Weldon, de la revue \emph{Biometrika} (1902)"}
Les affirmations de Mendel se fondent sur des travaux s'étendant sur plus de huit ans. Les résultats obtenus, remarquables, valent amplement, bien qu'elle soit énorme, la quantité de travail qu'ils ont certainement occasionnée, et la question se pose immédiatement de la mesure dans laquelle les lois qui en ont été déduites sont d'une application générale. Il est connu de presque tous qu'elles ne sont pas vraies pour tous les caractères, même dans les pois, et Mendel ne suggère pas qu'elles le soient. Je ne vois toutefois pas comment éviter la conclusion qu'elles ne s'appliquent pas universellement pour les caractères des pois décrits par Mendel de façon si minutieuse. Dans mon effort pour condenser les éléments sur lesquels je fonde mon opinion, je n'ai aucunement le désir de dénigrer l'importance de ce que Mendel a accompli. Je souhaite simplement attirer l'attention sur une série de faits qui me semblent indiquer des pistes de recherche utiles.[^weldon1]
::::

[^weldon1]: W.F.R. Weldon, « Mendel's Laws of Alternative Inheritance in Peas », *Biometrika*, 1902, vol. 1, n^o^ 2, p. 228-254, <https://doi.org/10.1093/biomet/1.2.228>, p. 235. Traduction de l'anglais : Sandra Mouton.

Il est important de signaler que Mendel était très prudent dans son article. Il n'y parlait que de sept caractères d'une seule plante et il écrivait très clairement qu'il ne prétendait pas que tous les caractères se comporteraient exactement de la même manière. Pour prendre un exemple simple d'un trait qui, lui, fonctionne différemment, la taille humaine est très complexe parce qu'y interagissent de très nombreux facteurs (muscles, os, tendons, etc.) et qu'il n'y a donc pas moyen de voir la taille de quelqu'un comme le résultat d'un seul gène. Mendel n'aurait pas dit le contraire et ce point n'était pas controversé.

![Représentation de W.F.R. Weldon des caractères dans différentes souches de pois (image : domaine public, dans Weldon \[1902\], image scannée par Charles Pence)](weldon_peas.jpg){.lgimg width=100%}

Ce qui l'était en revanche est la position de Weldon selon laquelle, même dans les pois et **même pour certains des caractères étudiés par Mendel**, l'explication de celui-ci ne pouvait pas fonctionner. L'image ci-dessus est une photo préparée par Weldon de pois différents qu'il avait récoltés lui-même, après avoir écrit à des fournisseurs de semences agricoles de toute l'Angleterre demandant différents types de pois. Le résultat était surprenant.

:::: think
**RÉFLEXION 4 :** Faire insérer cette photographie en couleur à son article scientifique était à la fois difficile et coûteux à l'époque. Il est donc clair que Weldon cherchait à persuader son lectorat d'une façon spécifique. En quoi une photo pouvait être plus persuasive qu'une simple description ? Qu'est-ce que Weldon cherchait à montrer à propos de lui-même ou de son protocole scientifique en incluant cette image ?
::::

Arrêtons-nous sur la couleur un moment. Souvenez-vous, dans l'explication de Mendel, les pois sont soit verts soit jaunes, selon qu'ils ont les allèles vert ou jaune, et le jaune est dominant tandis que le vert est récessif. Maintenant, regardez la première rangée de pois, numérotés de 1 à 6. Weldon avait trouvé, au sein d'une seule variété de pois, des exemples semblant passer en dégradé du vert pur (numéro 1) à un jaune orangé (numéro 6). D'ailleurs, il avait même trouvé des pois qui ne paraissaient pas d'une seule couleur uniforme (numéros 13 à 18).

Le même problème se représente pour tous les traits étudiés par Mendel. Comme le dit Weldon :

:::: {.quote title="Weldon, de la revue \emph{Biometrika} (1902)"}
De nombreuses souches de pois sont extrêmement variables, et en couleur et en forme. Une souche aux grains « ronds et lisses », par exemple, ne produit pas des grains exactement identiques. Au contraire, de nombreux grains \[\...\] présentent des irrégularités considérables tandis que dans \[d'autres\], on trouve à peine deux grains qui se ressemblent. Ainsi et la catégorie « ronds et lisses » et celle « ridés et irréguliers » incluent une gamme considérable de variétés. Parallèlement, les catégories sont, sans doute aucun, très souvent discontinues : le plus ridé des grains de \[certains pois\] est tellement plus lisse et plus arrondi que le plus régulier des souches typiquement « ridées » que personne, qui connaîtrait les deux souches, n'hésiterait un instant avant de dire à quelle souche un grain donné ressemble.[^weldon2]
::::

[^weldon2]: Weldon, « Mendel's Laws of Alternative Inheritance in Peas », p. 236.

Weldon identifie ici deux problèmes. D'abord, les pois qu'il a étudiés sont souvent très **variables**. Une population exclusivement constituée de pois à grains « ronds » présente des grains qui sont assez différents les uns des autres. Il semblerait que le trait « rond » puisse s'exprimer de bien des manières. Deuxième problème, la signification de « rond » et de « lisse » peut être différente d'une sorte de pois à l'autre. Si vous aviez consacré un long moment à observer une variété de pois, au point de penser que vous maîtrisez la définition de « lisse », et que vous vous mettiez à observer une variété différente, vous pourriez bien constater que même le plus ridé des pois de votre nouvelle variété aurait été qualifié de « lisse » auparavant.

En bref, la première critique majeure par Weldon à propos de Mendel est que les catégories fixées par celui-ci ne correspondent pas à la réalité biologique : il faut se donner des manières plus complexes de décrire le monde si on veut appréhender toute l'étendue de la diversité même de quelque chose d'aussi simple que la couleur des pois.

:::: think
**RÉFLEXION 5 :** Si les données sont ici aussi incertaines et variables que Weldon le dit, nous sommes face à un défi intéressant. Comment faire de la science, malgré ces problèmes dans nos données ? Comment éviter d'interpréter les données dont nous disposons selon nos convictions antérieures (biais de confirmation) ? Quels types de techniques pourraient nous aider à résoudre ce problème ?
::::

\clearpage

![Pigeons « de concours » (image : domaine public, illustration d'Anton Schöner, Wikimedia Commons)](pigeons.jpg){.medimg width=66% footnote="Emil Schachtzabel, *Illustriertes Prachtwerk sämtlicher Taubenrassen*, Würzburg, 1906, éd. Königl. Universitätsdruckerei H. Stürtz A. G."}

Le deuxième sujet de préoccupation de Weldon est lié à un point dont discutaient abondamment les biologistes de l'évolution depuis la place importante qu'il avait pris dans le travail de Darwin. À cette époque, les biologistes avaient amassé beaucoup de données sur un phénomène très étrange. Parfois, dans la culture d'un type spécifique de plante ou l'élevage d'un animal très particulier (comme ces pigeons « de compétition ») et ce même si les générations sont nombreuses et s'étendent sur une longue période, il arrive que se manifeste subitement [ce qu'on a appelé une « réversion » ou un « atavisme »](https://www.sciencedirect.com/topics/biochemistry-genetics-and-molecular-biology/atavism), c'est-à-dire une situation où l'organisme revient brusquement à l'apparence d'un ancêtre très éloigné (comme le genre de pigeon qu'on voit dans la rue).

:::: think
**RÉFLEXION 6 :** Quels types d'explications possibles voyez-vous à ce phénomène ?
::::

Outre le fait que c'est très étrange, cela a donné à Weldon l'idée qu'il fallait penser à l'effet qu'ont **les ancêtres éloignés** sur les organismes en vie, **ce qui n'est pas** compatible avec la théorie de Mendel. Pour Mendel, le seul facteur important est les gènes possédés par vos parents, pas besoin de remonter plus loin en arrière pour comprendre les gènes que vous aurez vous :

:::: {.quote title="Weldon, de la revue \emph{Biometrika} (1902)"}
Mendel traite les caractères comme la couleur jaune des cotylédons, et autres caractères semblables, comme si l'état du caractère chez deux parents donnés déterminait son état chez tous leurs descendants. Or il est bien connu des éleveurs \[\...\] que la constitution d'un animal ne dépend pas, de manière systématique, de celle d'une paire unique d'ancêtres mais à divers degrés de la constitution de tous ses ancêtres de toutes les générations précédentes, celle de chaque génération de la demi-douzaine la plus proche ayant un effet assez sensible.[^weldon3]
::::

[^weldon3]: Weldon, « Mendel's Laws of Alternative Inheritance in Peas », p. 241.

Vu d'aujourd'hui, nous comprenons mieux ce qui peut causer ce type d'effets. Les réseaux de gènes (dont nous avons brièvement parlé précédemment, où [de très nombreux gènes interagissent entre eux dans des relations complexes de régulation et de contrôle](https://en.wikipedia.org/wiki/Gene_regulatory_network)) pourraient produire exactement ce type de résultats où des gènes qui avaient été « désactivés » pendant un certain nombre de générations étaient soudainement réactivés. Mais cette réponse ne nous est disponible que maintenant, ni Mendel ni Weldon n'avait jamais imaginé que c'était aussi compliqué. Ce dont Weldon était certain, en revanche, est qu'il y avait forcément plus de facteurs en jeu que Mendel n'en identifiait.

:::: {.quote title="Weldon, de la revue \emph{Biometrika} (1902)"}
Ces exemples, choisis parmi bien d'autres qu'on aurait pu citer, me semblent montrer qu'il n'est pas possible de conclure que la dominance est une propriété d'un caractère, simplement en se fondant sur sa présence chez un des deux parents. Le degré auquel un caractère parental affecte la descendance dépend non seulement de son développement chez ce parent en particulier mais aussi de son degré de développement chez les ancêtres de ce parent.[^weldon4]
::::

[^weldon4]: Weldon, « Mendel's Laws of Alternative Inheritance in Peas », p. 244.

La conclusion à laquelle arrive Weldon en se basant sur la réversion est que la dominance ne peut pas être aussi simple que la description de Mendel. Si l'expression d'un caractère ou sa non-expression dans la descendance met en jeu plus que la présence ou l'absence de ce caractère chez les parents, alors quelque chose d'autre que la simple dominance contrôle l'expression des caractères. Il en découle que la dominance est plus qu'une simple propriété des caractères et qu'il faut dresser un tableau de la génétique plus complexe que « jaune est dominant et vert est récessif ». Si la phrase précédente est vraie, « être dominant » ne relève pas du simple fait concernant l'allèle jaune.

Mais si le lien entre les gènes possédés par un organisme et les caractères qu'il présente n'est pas uniquement une question de dominance, comment Weldon pense-t-il que nous devrions le comprendre ? Comme mentionné plus haut, Weldon a été un des premiers parmi les biologistes à plaider sérieusement en faveur de l'utilisation des statistiques en théorie de l'évolution. Si nous voulons comprendre des caractères comme la taille chez l'être humain, nous savons qu'il sera impossible de penser en termes de « gène pour grande taille » et « gène pour petite taille ». Nous devons examiner la [courbe qui décrit la taille de toutes les personnes](https://fr.wikipedia.org/wiki/Loi_normale), avec beaucoup de gens de taille moyenne au milieu et quelques personnes très grandes et très petites aux deux extrémités.[^sex]

[^sex]: Pour vraiment comprendre la taille humaine, il y a beaucoup plus à faire : il faut aussi expliquer le fait que les hommes sont habituellement plus grands que les femmes, que les personnes qui mangent mieux pendant l'enfance sont plus grandes que celles qui sont mal-nourries et de nombreux autres facteurs.

:::: think
**RÉFLEXION 7 :** Si votre conviction était que la plupart des caractères sont effectivement transmis comme ceux des pois de Mendel, quel autre type d'explication pourriez-vous donner pour les caractères comme la taille, qui semblent être distribués de façon très différente ? Quels types de preuves faudrait-il pour trancher entre l'explication de Weldon et la vôtre ?
::::

Weldon avance donc que Mendel a fait erreur précisément parce qu'il ne traite pas les caractères comme ces courbes statistiques. Il a décrit ainsi son objection dans une lettre à un confrère en 1902 :

:::: {.quote title="Weldon, lettre à Pearson (1902)"}
Ce que tous les mendéliens font est de prendre le diagramme de fréquence

![](graph.png){.lgimg width=100%}

et de dire qu'un intervalle AB est un « caractère » et l'intervalle BC l'autre « caractère » d'une paire mendélienne.

Maintenant, prenez un parent n'importe où du côté A de la valeur moyenne et faites-le se reproduire avec un parent de l'intervalle BC, pour toute famille de taille expérimentale\[ment pertinente\], la probabilité qu'il se présente des descendants tombant dans l'intervalle BC est forcément proche de 0.

Ceci vous donne immédiatement une « dominance imparfaite » d'AB sur BC.[^letter]
::::

[^letter]: Cette lettre, écrite le 23 juin 1902, se trouve dans le fonds d'[archives Karl Pearson à University College, London](https://archives.ucl.ac.uk/CalmView/Record.aspx?src=CalmView.Catalog&id=PEARSON) dans le carton PEARSON/11/1/22/40.7.3. Traduction de l'anglais : Sandra Mouton.

Regardons la courbe qu'il a tracée et imaginons qu'elle décrit la couleur des pois, comme sur la rangée du haut dans la photo ci-dessus. Weldon dit que ce caractère doit être vu comme une courbe, exactement comme pour la taille chez les êtres humains. Si nous en avons envie, nous pourrions dire que « vert » et « jaune » sont des caractères clairement définis, ce qui signifierait diviser la courbe et séparer les pois extrêmement verts des pois moins extrêmes, plus jaunes. Selon Weldon, nous pourrions dire la même chose des caractères distribués de façon continue, tels que la taille.

Pensez aux enfants d'une mère très grande et d'un père très petit : si les relations statistiques entre les parents se combinent de façon favorable, le résultat pourrait être comparable à la transmission mendélienne. Mais la réalité derrière cet aspect extérieur serait, elle, bien plus compliquée. Weldon essayait de dire qu'il se passait exactement la même chose dans le cas des pois. On pourrait certes simplifier d'une certaine manière pour obtenir les proportions de Mendel mais ils ne seraient, dans un certain sens, rien de plus que des outils théoriques utiles, au lieu d'être des reflets de la réalité du monde.

:::: think
**RÉFLEXION 8 :** Un des enjeux dans la réponse de Weldon concerne la fonction des théories scientifiques. Qu'est-ce qui nous pousse à les élaborer ? Est-ce que nous voulons seulement les utiliser pour faire des prédictions sur le monde qui nous entoure ou les théories scientifiques sont-elles censées nous dire de quoi le monde est fait et comment ses éléments se combinent ? Pensez-vous qu'une de ces possibilités nous donne une « meilleure » science ? Laquelle ?
::::

Bien sûr, nous savons aujourd'hui que, dans un sens, c'était tout à fait vrai : la réalité sous-jacente était bien plus complexe encore que Weldon (ou quiconque) ne l'avait réalisé à l'époque. Quels enseignements pour la biologie contemporaine pouvons-nous tirer de la prudence de Weldon ?

Plus important, nous devons faire attention à ne pas tirer trop de conclusions des pois de Mendel. Certes, nous savons maintenant que Mendel avait raison. Les caractères qu'il étudiait étaient réellement le résultat de changements sur un seul gène et il y avait effectivement une dominance quasi complète dans le cas des pois qu'il a étudiés. Cependant, comme Weldon l'avait pressenti, c'est un [cas extrêmement inhabituel dans la nature](https://fr.wikipedia.org/wiki/Dominance_(g%C3%A9n%C3%A9tique)). Très peu de caractères fonctionnent comme ça. Par conséquent, nous devons faire très attention quand nous entendons parler du [« gène pour » un caractère en particulier](https://dx.doi.org/10.1002%2Fbies.200800133). Très souvent, surtout dans les médias, la nouvelle qu'il existe un lien statistique entre un gène et un résultat très important pour le grand public est médiatisée comme « la découverte du gène codant pour » telle ou telle maladie physique ou trouble mental. C'est exactement contre ce genre de saut des gènes à des résultats compliqués que Weldon voulait nous avertir.

:::: think
**RÉFLEXION 9 :** Avez-vous déjà personnellement vu des exemples de ce type dans les médias ? Par exemple, disons que quelqu'un déclare avoir découvert « le gène de l'obésité ». Comment approcheriez-vous l'évaluation critique de ce type d'allégation ? De quelles preuves complémentaires aurait-on d'après vous besoin pour savoir si c'est vrai ?
::::

La critique menée par Weldon peut attirer notre attention sur un autre facteur, qui rend les raisons d'apparition des résultats chez les organismes beaucoup plus compliquées que les schémas mendéliens simples ne le laissent penser : [la relation entre les gènes et l'environnement](https://en.wikipedia.org/wiki/Gene–environment_interaction) car, bien sûr, les gènes ne font rien tout seuls. Une caractéristique réelle, présente dans un organisme, résulte de l'expression d'un gène au cours de toute la vie de l'individu, qui vit, mange et interagit dans un environnement qui comprend d'autres organismes, des ressources naturelles, de la nourriture, des représentants d'autres espèces et d'innombrables autres influences. Toutes ces interactions, pas seulement les gènes, ont leur importance pour la vie future des organismes.

:::: think
**RÉFLEXION 10 :** Imaginez que nous soyons face à deux explications concurrentes du même phénomène, une explication génétique et une environnementale. Comment différencier entre elles ? Qu'est-ce que vous voudriez savoir pour pouvoir choisir ?
::::

On peut entrapercevoir l'importance de ce point à l'intérieur même de la critique de Weldon. Bien que nous n'ayons pas le temps de faire ici tout l'historique de l'idée de réversion, une autre raison de la réapparition en milieu naturel de caractères hérités d'ancêtres lointains est que les effets environnementaux sur les organismes peuvent se produire à nouveau : être élevés dans un environnement particulier, le même que pour un de leurs ancêtres, peut causer l'expression de traits qu'on n'avait pas vus se manifester depuis cet ancêtre.

En bref, depuis la naissance de la génétique, il existe des tiraillements entre la recherche d'explications génétiques simples à des phénomènes complexes --- on en trouve à l'occasion --- et les approches appelant à la prudence sur le rôle des gènes et de leurs relations à la fois entre eux et avec l'environnement. Il est crucial que nous réfléchissions bien aux conséquences de ceci, en particulier quand des questions importantes sur [la chaîne alimentaire](https://www.who.int/foodsafety/areas_work/food-technology/faq-genetically-modified-food/en/) ou [la santé humaine](https://www.genome.gov/For-Patients-and-Families/Genetic-Disorders) sont en jeu.


## RÉFLEXION : questions sur la nature des sciences

Qu'est-ce que l'histoire de la réponse de Weldon à Mendel nous dit sur les aspects suivants des sciences ?

* robustesse (concordance entre de nombreux types différents de données)
* biais de confirmation
* réflexion interdisciplinaire
* nouvelles techniques et leur validation
* formes de persuasion
* erreur et incertitude
* crédibilité des médias d'information


## Lectures d'approfondissement

* Karl Pearson, « Walter Frank Raphael Weldon. 1860--1906 », *Biometrika*, 1906, vol. 5, n^o^ 1-2, p. 1-52, <https://doi.org/10.1093/biomet/5.1-2.1>.
* Gregory Radick, « Other Histories, Other Biologies », *Royal Institute of Philosophy Supplement*, 2005, vol. 56, n^o^ 3-4, p. 21-47, <https://doi.org/10.1017/S135824610505602X>.
* Gregory Radick, « Teach Students the Biology of Their Time », *Nature*, 2016, vol. 533, n^o^ 7603, p. 293, <https://doi.org/10.1038/533293a>.
