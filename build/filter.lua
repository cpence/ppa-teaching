if FORMAT:match 'html' then

  function Div(el)
    if el.classes:includes("quote") then
      -- parse markup in the title
      title = pandoc.read(el.attr.attributes["title"], "latex")
      titlediv = pandoc.Div(title.blocks)
      titlediv.attr.attributes["class"] = "quote-title"

      wrapper = pandoc.Div({
        titlediv,
        el
      })
      wrapper.attr.attributes["class"] = "quote-wrapper"
      return wrapper
    end
    return el
  end

  function Image(el)
    -- add the footnote if needed
    local note = el.attr.attributes["footnote"]
    if note then
      local note_content = pandoc.read(note, "markdown")
      local note_elt = pandoc.Note(note_content.blocks)
      table.insert(el.caption, note_elt)
    end

    -- doctor the image path
    el.src = "/images/lessons/" .. el.src

    return el
  end

end

if FORMAT:match 'docx' then

  function Image(el)
    -- add the footnote if needed
    local note = el.attr.attributes["footnote"]
    if note then
      local note_content = pandoc.read(note, "markdown")
      local note_elt = pandoc.Note(note_content.blocks)
      table.insert(el.caption, note_elt)
    end

    return el
  end

end

if FORMAT:match 'latex' then

  local function latex(str)
    return pandoc.RawInline('latex', str)
  end

  function Div(el)
    if el.classes:includes("think") then
      -- insert element in front
      table.insert(
        el.content, 1,
        pandoc.RawBlock("latex", "\\begin{mdframed}[backgroundcolor=boxbg,splittopskip=0.5cm,skipabove=\\cpathinkskip]"))
      -- insert element at the back
      table.insert(
        el.content,
        pandoc.RawBlock("latex", "\\end{mdframed}"))
    elseif el.classes:includes("quote") then
      -- insert element in front
      table.insert(
        el.content, 1,
        pandoc.RawBlock("latex", "\\begin{parchment}[" .. el.attr.attributes["title"] .. "]"))
      -- insert element at the back
      table.insert(
        el.content,
        pandoc.RawBlock("latex", "\\end{parchment}"))
    end
    return el
  end

  function Image(el)
    local lr = "r"
    local width = "0.333\\textwidth"

    local note = el.attr.attributes["footnote"]
    local note_content = pandoc.Str("")
    if note then
      -- get to the list of inlines inside this single paragraph
      note_content = pandoc.read(note, "markdown").blocks[1].content
      table.insert(note_content, 1, latex("\\footnotetext{"))
      table.insert(note_content, latex("}"))

      table.insert(el.caption, latex("\\protect\\footnotemark{}"))
    end

    if el.classes:includes("smallimg") or el.classes:includes("medimg") then
      if el.classes:includes("left") then
        lr = "l"
      end
      if el.classes:includes("medimg") then
        width = "0.666\\textwidth"
      end

      return {
        latex("\\begin{wrapfigure}{" .. lr .. "}{" .. width .. "}"),
        el,
        latex("\\caption"),
        pandoc.Span(el.caption),
        latex("\\end{wrapfigure}"),
        pandoc.Span(note_content)
      }
    elseif el.classes:includes("lgimg") then
      if #el.caption > 0 then
        return {
          latex("\\vspace{10pt}\\begingroup\\noindent"),
          el,
          latex("\\captionof{figure}"),
          pandoc.Span(el.caption),
          latex("\\endgroup\\vspace{\\cpabigfigskip}"),
          pandoc.Span(note_content)
        }
      else
        return {
          latex("\\vspace{10pt}"),
          el,
          latex("\\vspace{10pt}")
        }
      end
    end
    return el
  end

end
